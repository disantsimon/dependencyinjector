/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class InjectedContainer {
}
InjectedContainer.injected = {};
/**
 * @template T
 * @param {?} Service
 * @return {?}
 */
function Resolve(Service) {
    if (InjectedContainer.injected[Service.name] === undefined) {
        console.warn("NO SERVICE NAMED " + Service.name);
    }
    return InjectedContainer.injected[Service.name];
}
/**
 * @param {?} Service
 * @param {?=} singleton
 * @return {?}
 */
function Inject(Service, singleton = true) {
    return function (target, propertyName) {
        try {
            InjectedContainer.injected[Service.name] = InjectedContainer.injected[Service.name] || new Service();
            target[propertyName] = singleton ? InjectedContainer.injected[Service.name] : new Service();
        }
        catch (e) {
            console.warn("COULD NOT INIZIALIZE " + Service);
        }
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { Resolve, Inject };

//# sourceMappingURL=dependency-injector.js.map