export declare function Resolve<T>(Service: any): T;
export declare function Inject(Service: any, singleton?: boolean): (target: any, propertyName: any) => void;
