(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
    typeof define === 'function' && define.amd ? define('dependency-injector', ['exports'], factory) :
    (factory((global['dependency-injector'] = {})));
}(this, (function (exports) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var InjectedContainer = /** @class */ (function () {
        function InjectedContainer() {
        }
        InjectedContainer.injected = {};
        return InjectedContainer;
    }());
    /**
     * @template T
     * @param {?} Service
     * @return {?}
     */
    function Resolve(Service) {
        if (InjectedContainer.injected[Service.name] === undefined) {
            console.warn("NO SERVICE NAMED " + Service.name);
        }
        return InjectedContainer.injected[Service.name];
    }
    /**
     * @param {?} Service
     * @param {?=} singleton
     * @return {?}
     */
    function Inject(Service, singleton) {
        if (singleton === void 0) {
            singleton = true;
        }
        return function (target, propertyName) {
            try {
                InjectedContainer.injected[Service.name] = InjectedContainer.injected[Service.name] || new Service();
                target[propertyName] = singleton ? InjectedContainer.injected[Service.name] : new Service();
            }
            catch (e) {
                console.warn("COULD NOT INIZIALIZE " + Service);
            }
        };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.Resolve = Resolve;
    exports.Inject = Inject;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=dependency-injector.umd.js.map