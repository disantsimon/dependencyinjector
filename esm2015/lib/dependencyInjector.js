/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class InjectedContainer {
}
InjectedContainer.injected = {};
if (false) {
    /** @type {?} */
    InjectedContainer.injected;
}
/**
 * @template T
 * @param {?} Service
 * @return {?}
 */
export function Resolve(Service) {
    if (InjectedContainer.injected[Service.name] === undefined) {
        console.warn("NO SERVICE NAMED " + Service.name);
    }
    return InjectedContainer.injected[Service.name];
}
/**
 * @param {?} Service
 * @param {?=} singleton
 * @return {?}
 */
export function Inject(Service, singleton = true) {
    return function (target, propertyName) {
        try {
            InjectedContainer.injected[Service.name] = InjectedContainer.injected[Service.name] || new Service();
            target[propertyName] = singleton ? InjectedContainer.injected[Service.name] : new Service();
        }
        catch (e) {
            console.warn("COULD NOT INIZIALIZE " + Service);
        }
    };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVwZW5kZW5jeUluamVjdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZGVwZW5kZW5jeS1pbmplY3Rvci8iLCJzb3VyY2VzIjpbImxpYi9kZXBlbmRlbmN5SW5qZWN0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE1BQU0saUJBQWlCOztBQUVaLDBCQUFRLEdBQUcsRUFFakIsQ0FBQTs7O0lBRkQsMkJBRUM7Ozs7Ozs7QUFHTCxNQUFNLFVBQVUsT0FBTyxDQUFJLE9BQU87SUFDOUIsSUFBSSxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLFNBQVMsRUFBRTtRQUN4RCxPQUFPLENBQUMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUNwRDtJQUNELE9BQU8saUJBQWlCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUNwRCxDQUFDOzs7Ozs7QUFFRCxNQUFNLFVBQVUsTUFBTSxDQUFDLE9BQU8sRUFBRSxTQUFTLEdBQUcsSUFBSTtJQUM1QyxPQUFPLFVBQVUsTUFBTSxFQUFFLFlBQVk7UUFDakMsSUFBSTtZQUNBLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsaUJBQWlCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLE9BQU8sRUFBRSxDQUFDO1lBQ3JHLE1BQU0sQ0FBQyxZQUFZLENBQUMsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksT0FBTyxFQUFFLENBQUM7U0FDL0Y7UUFDRCxPQUFPLENBQUMsRUFBRTtZQUNOLE9BQU8sQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsT0FBTyxDQUFDLENBQUM7U0FDbkQ7SUFFTCxDQUFDLENBQUE7QUFDTCxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiY2xhc3MgSW5qZWN0ZWRDb250YWluZXIge1xyXG5cclxuICAgIHN0YXRpYyBpbmplY3RlZCA9IHtcclxuXHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBSZXNvbHZlPFQ+KFNlcnZpY2UpOiBUIHtcclxuICAgIGlmIChJbmplY3RlZENvbnRhaW5lci5pbmplY3RlZFtTZXJ2aWNlLm5hbWVdID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICBjb25zb2xlLndhcm4oXCJOTyBTRVJWSUNFIE5BTUVEIFwiICsgU2VydmljZS5uYW1lKTtcclxuICAgIH1cclxuICAgIHJldHVybiBJbmplY3RlZENvbnRhaW5lci5pbmplY3RlZFtTZXJ2aWNlLm5hbWVdO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gSW5qZWN0KFNlcnZpY2UsIHNpbmdsZXRvbiA9IHRydWUpIHtcclxuICAgIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0LCBwcm9wZXJ0eU5hbWUpIHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBJbmplY3RlZENvbnRhaW5lci5pbmplY3RlZFtTZXJ2aWNlLm5hbWVdID0gSW5qZWN0ZWRDb250YWluZXIuaW5qZWN0ZWRbU2VydmljZS5uYW1lXSB8fCBuZXcgU2VydmljZSgpO1xyXG4gICAgICAgICAgICB0YXJnZXRbcHJvcGVydHlOYW1lXSA9IHNpbmdsZXRvbiA/IEluamVjdGVkQ29udGFpbmVyLmluamVjdGVkW1NlcnZpY2UubmFtZV0gOiBuZXcgU2VydmljZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLndhcm4oXCJDT1VMRCBOT1QgSU5JWklBTElaRSBcIiArIFNlcnZpY2UpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcbn1cclxuIl19