/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var InjectedContainer = /** @class */ (function () {
    function InjectedContainer() {
    }
    InjectedContainer.injected = {};
    return InjectedContainer;
}());
if (false) {
    /** @type {?} */
    InjectedContainer.injected;
}
/**
 * @template T
 * @param {?} Service
 * @return {?}
 */
export function Resolve(Service) {
    if (InjectedContainer.injected[Service.name] === undefined) {
        console.warn("NO SERVICE NAMED " + Service.name);
    }
    return InjectedContainer.injected[Service.name];
}
/**
 * @param {?} Service
 * @param {?=} singleton
 * @return {?}
 */
export function Inject(Service, singleton) {
    if (singleton === void 0) { singleton = true; }
    return function (target, propertyName) {
        try {
            InjectedContainer.injected[Service.name] = InjectedContainer.injected[Service.name] || new Service();
            target[propertyName] = singleton ? InjectedContainer.injected[Service.name] : new Service();
        }
        catch (e) {
            console.warn("COULD NOT INIZIALIZE " + Service);
        }
    };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVwZW5kZW5jeUluamVjdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZGVwZW5kZW5jeS1pbmplY3Rvci8iLCJzb3VyY2VzIjpbImxpYi9kZXBlbmRlbmN5SW5qZWN0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBO0lBQUE7SUFLQSxDQUFDO0lBSFUsMEJBQVEsR0FBRyxFQUVqQixDQUFBO0lBQ0wsd0JBQUM7Q0FBQSxBQUxELElBS0M7OztJQUhHLDJCQUVDOzs7Ozs7O0FBR0wsTUFBTSxVQUFVLE9BQU8sQ0FBSSxPQUFPO0lBQzlCLElBQUksaUJBQWlCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxTQUFTLEVBQUU7UUFDeEQsT0FBTyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDcEQ7SUFDRCxPQUFPLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDcEQsQ0FBQzs7Ozs7O0FBRUQsTUFBTSxVQUFVLE1BQU0sQ0FBQyxPQUFPLEVBQUUsU0FBZ0I7SUFBaEIsMEJBQUEsRUFBQSxnQkFBZ0I7SUFDNUMsT0FBTyxVQUFVLE1BQU0sRUFBRSxZQUFZO1FBQ2pDLElBQUk7WUFDQSxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxPQUFPLEVBQUUsQ0FBQztZQUNyRyxNQUFNLENBQUMsWUFBWSxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLE9BQU8sRUFBRSxDQUFDO1NBQy9GO1FBQ0QsT0FBTyxDQUFDLEVBQUU7WUFDTixPQUFPLENBQUMsSUFBSSxDQUFDLHVCQUF1QixHQUFHLE9BQU8sQ0FBQyxDQUFDO1NBQ25EO0lBRUwsQ0FBQyxDQUFBO0FBQ0wsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImNsYXNzIEluamVjdGVkQ29udGFpbmVyIHtcclxuXHJcbiAgICBzdGF0aWMgaW5qZWN0ZWQgPSB7XHJcblxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gUmVzb2x2ZTxUPihTZXJ2aWNlKTogVCB7XHJcbiAgICBpZiAoSW5qZWN0ZWRDb250YWluZXIuaW5qZWN0ZWRbU2VydmljZS5uYW1lXSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgY29uc29sZS53YXJuKFwiTk8gU0VSVklDRSBOQU1FRCBcIiArIFNlcnZpY2UubmFtZSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gSW5qZWN0ZWRDb250YWluZXIuaW5qZWN0ZWRbU2VydmljZS5uYW1lXTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIEluamVjdChTZXJ2aWNlLCBzaW5nbGV0b24gPSB0cnVlKSB7XHJcbiAgICByZXR1cm4gZnVuY3Rpb24gKHRhcmdldCwgcHJvcGVydHlOYW1lKSB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgSW5qZWN0ZWRDb250YWluZXIuaW5qZWN0ZWRbU2VydmljZS5uYW1lXSA9IEluamVjdGVkQ29udGFpbmVyLmluamVjdGVkW1NlcnZpY2UubmFtZV0gfHwgbmV3IFNlcnZpY2UoKTtcclxuICAgICAgICAgICAgdGFyZ2V0W3Byb3BlcnR5TmFtZV0gPSBzaW5nbGV0b24gPyBJbmplY3RlZENvbnRhaW5lci5pbmplY3RlZFtTZXJ2aWNlLm5hbWVdIDogbmV3IFNlcnZpY2UoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKFwiQ09VTEQgTk9UIElOSVpJQUxJWkUgXCIgKyBTZXJ2aWNlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG59XHJcbiJdfQ==